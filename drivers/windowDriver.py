import Adafruit_DHT
import datetime
from w1thermsensor import W1ThermSensor
import RPi.GPIO as GPIO
import logging
import time

STATUS_FILE_PATH = '/home/pi/STATUS'
WINDOW_CLOSED=0
WINDOW_OPEN=0.8
WINDOW_OPEN_EXTRA=1.0
WINDOW_PIN1=6
WINDOW_PIN2=5

def changeWindowState(prev, curr):
    if curr > prev:
        openWindow((curr-prev)*18)
    else:
        closeWindow((prev-curr)*18)

def setupWindow(s1, s2):
    GPIO.setup(WINDOW_PIN1, GPIO.OUT)
    GPIO.setup(WINDOW_PIN2, GPIO.OUT)
    GPIO.output(WINDOW_PIN1, s1)
    GPIO.output(WINDOW_PIN2, s2)

def openWindow(period):
    logging.info("Open window for " + str(period))
    setupWindow(GPIO.HIGH, GPIO.LOW)
    time.sleep(period)
    GPIO.output(WINDOW_PIN1, GPIO.LOW)

def closeWindow(period):
    logging.info("Close window for " + str(period))
    setupWindow(GPIO.LOW, GPIO.HIGH)
    time.sleep(period)
    GPIO.output(WINDOW_PIN2, GPIO.LOW)

# 
# Get previous values
#
previousFanState = 0
previousWindowState = 0.0
with open(STATUS_FILE_PATH, "r") as statusFile:
    (a,b) = statusFile.read().split(",")
    previousFanState = int(a)
    previousWindowState = float(b)



GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

windowState = 0.3

with open(STATUS_FILE_PATH, "w+") as statusFile:
    statusFile.write(str(previousFanState) + "," + str(windowState))

changeWindowState(previousWindowState, windowState)

