import Adafruit_DHT
import datetime
from w1thermsensor import W1ThermSensor
import RPi.GPIO as GPIO
import logging
import time

STATUS_FILE_PATH = '/home/pi/STATUS'
FAN_ON=1
FAN_OFF=0
FAN_PIN=22

def fanOutput(state):
    if state == FAN_ON:
        return GPIO.LOW
    else:
        return GPIO.HIGH

# 
# Get previous values
#
previousFanState = 0
previousWindowState = 0.0
with open(STATUS_FILE_PATH, "r") as statusFile:
    (a,b) = statusFile.read().split(",")
    previousFanState = int(a)
    previousWindowState = float(b)

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

#
# Set new fan status
#
fanState = FAN_OFF
GPIO.setup(FAN_PIN, GPIO.OUT)
GPIO.output(FAN_PIN, fanOutput(fanState))

#
# Save new status
#
with open(STATUS_FILE_PATH, "w+") as statusFile:
    statusFile.write(str(fanState) + "," + str(previousWindowState))


