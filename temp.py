import Adafruit_DHT
import datetime
from w1thermsensor import W1ThermSensor
import RPi.GPIO as GPIO
import logging
import time
from subprocess import Popen

logging.basicConfig(filename='temp.log',level=logging.DEBUG)
STATUS_FILE_PATH = '/home/pi/STATUS'
DATA_FILE_PATH = '/home/pi/data.csv'
FAN_ON=1
FAN_OFF=0

FAN_PIN=22
WINDOW_PIN1=6
WINDOW_PIN2=5

WATERING_SYSTEM_ENABLED=True

def fanOutput(state):
    if state == FAN_ON:
        return GPIO.LOW
    else:
        return GPIO.HIGH

def changeWindowState(prev, curr):
    if curr > prev:
        openWindow((curr-prev)*18.0)
    else:
        closeWindow((prev-curr)*19.0)

def setupWindow(s1, s2):
    GPIO.setup(WINDOW_PIN1, GPIO.OUT)
    GPIO.setup(WINDOW_PIN2, GPIO.OUT)
    GPIO.output(WINDOW_PIN1, s1)
    GPIO.output(WINDOW_PIN2, s2)

def openWindow(period):
    logging.info("Open window for " + str(period))
    setupWindow(GPIO.HIGH, GPIO.LOW)
    time.sleep(period)
    GPIO.output(WINDOW_PIN1, GPIO.LOW)

def closeWindow(period):
    logging.info("Close window for " + str(period))
    setupWindow(GPIO.LOW, GPIO.HIGH)
    time.sleep(period)
    GPIO.output(WINDOW_PIN2, GPIO.LOW)

def getTemp(line):
    return float(line.split(',')[3])

def getTempDirection():
    with open(DATA_FILE_PATH, "r") as dataFile:
        lines = dataFile.readlines()[-4:]
        return getTemp(lines[2])+getTemp(lines[3]) - getTemp(lines[0])-getTemp(lines[1])
        

#
# Read values from sensors
#
try:
    sensor1 = W1ThermSensor(sensor_id="00000d95f2d6")
    s1_temp = sensor1.get_temperature()
except:
    s1_temp = 20
h1, t1 = Adafruit_DHT.read_retry(Adafruit_DHT.DHT22, 17) 
tstr = "null"
hstr = "null"
if h1 is not None and h1 < 100.0:
    tstr = "{0:0.1f}".format(t1)
    hstr = "{0:0.1f}".format(h1)
else:
    t1 = s1_temp

# 
# Get previous values
#
previousFanState = 0
previousWindowState = 0.0
with open(STATUS_FILE_PATH, "r") as statusFile:
    (a,b) = statusFile.read().split(",")
    previousFanState = int(a)
    previousWindowState = float(b)

#
# Logging
#
execTime = datetime.datetime.now().strftime("%d/%m/%y %H:%M")
logging.info("Execution at " + execTime)
logging.info("Current temp " + str(s1_temp) + ", " + str(t1))
logging.info("Previous fan state: " + str(previousFanState) + "       window state: " + str(previousWindowState))

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

#
# Set new fan status
#
fanState = 0
temp = s1_temp*0.35 + t1*0.65 + getTempDirection()*0.12
if(temp < 8+previousFanState*2 or temp > 30-previousFanState*2):
    fanState = 1
else:
    fanState = 0

if fanState != previousFanState:
    logging.info("Changing fan state to " + str(fanState))
    GPIO.setup(FAN_PIN, GPIO.OUT)
    GPIO.output(FAN_PIN, fanOutput(fanState))

#
# Set new window status
#
windowState = 0.0
logging.info("Predicted temperature: " + str(temp))
if temp > 31-previousWindowState*4:
    windowState = 1.0
elif temp > 26-previousWindowState*4:
    windowState = 0.7
elif temp > 22-previousWindowState*4:
    windowState = 0.3
else:
    windowState = 0.0
if windowState != previousWindowState:
    logging.info("Changing window state from " + str(previousWindowState) + " to " + str(windowState))
    changeWindowState(previousWindowState, windowState)

#
# Turn on/off water pump
#
if WATERING_SYSTEM_ENABLED:
    now = datetime.datetime.now()
    if now.hour == 9 and now.minute == 0:
        logging.info("Watering system execution")
        Popen(['python3', '/home/pi/greenhouse/watering.py'])
    if now.hour == 14 and now.minute == 0:
        logging.info("Watering system execution")
        Popen(['python3', '/home/pi/greenhouse/watering.py'])

#
# Save new status
#
with open(STATUS_FILE_PATH, "w+") as statusFile:
    statusFile.write(str(fanState) + "," + str(windowState))
logging.info("Current fan state: " + str(fanState) + "       window state: " + str(windowState))

print("{0},{1},{2},{3:0.1f},{4},{5}".format(execTime, hstr, tstr, s1_temp, fanState, windowState))

