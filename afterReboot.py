import datetime
import RPi.GPIO as GPIO
import logging
import time

logging.basicConfig(filename='temp.log',level=logging.DEBUG)
STATUS_FILE_PATH = '/home/pi/STATUS'
FAN_ON=1
FAN_OFF=0
FAN_PIN=22
PUMP_PIN=26

def fanOutput(state):
    if state == FAN_ON:
        return GPIO.LOW
    else:
        return GPIO.HIGH

# 
# Get previous values
#
previousFanState = 0
with open(STATUS_FILE_PATH, "r") as statusFile:
    (a,b) = statusFile.read().split(",")
    previousFanState = int(a)

#
# Logging
#
execTime = datetime.datetime.now().strftime("%d/%m/%y %H:%M")
logging.info("Reset after restart at " + execTime)
logging.info("Setting fan state: " + str(previousFanState))

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

#
# Turn fan off
#
GPIO.setup(FAN_PIN, GPIO.OUT)
GPIO.output(FAN_PIN, fanOutput(previousFanState))

#
# Turn pump off
# 
GPIO.setup(PUMP_PIN, GPIO.OUT)
GPIO.output(PUMP_PIN, 1)
