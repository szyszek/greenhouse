import datetime
from w1thermsensor import W1ThermSensor
import RPi.GPIO as GPIO
import logging
import time

logging.basicConfig(filename='watering.log',level=logging.DEBUG)

PUMP_PIN=12

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

#
# Turn on/off water pump
#
GPIO.setup(PUMP_PIN, GPIO.OUT)
nowTime = datetime.datetime.now().strftime("%d/%m/%y %H:%M")
logging.info("Water pump turned ON at " + nowTime)
GPIO.output(PUMP_PIN, 0)
time.sleep(360)
nowTime = datetime.datetime.now().strftime("%d/%m/%y %H:%M")
logging.info("Water pump turned OFF at " + nowTime)
GPIO.output(PUMP_PIN, 1)


